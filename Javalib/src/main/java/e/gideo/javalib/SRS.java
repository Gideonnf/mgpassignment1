package e.gideo.javalib;
import java.util.*;


public class SRS {
    static RecordManager Records;
    static Scanner S;
    public static void main(String[] args)
    {
        int choice;
        boolean Game = true;
        while(Game)
        {
            System.out.println("Hello Administrator");
            System.out.println("(1) Print Student List");
            System.out.println("(2) Add Student Record");
            System.out.println("(3) Remove Student Record");
            System.out.println("(4) Print Aggregated data");
            System.out.println("(0) Exit the program");
            System.out.println("Enter your choice");

            S = new Scanner(System.in);
            choice = S.nextInt();

            switch(choice)
            {
                case 0:
                    // Exit the program
                    Game = false;
                    break;
                case 1:
                    // Print Student List
                    System.out.println(Records.GetStudentList());
                    break;
                case 2:
                    // Add student
                    String name;
                    int RegNum;
                    int Gender;
                    float GPA;

                    System.out.println("Please key in Student's Name :");
                    S = new Scanner(System.in);
                    name = S.next();
                    System.out.println("Please key in Student's Reg Number (3 Digits):");
                    S = new Scanner(System.in);
                    RegNum = S.nextInt();
                    System.out.println("Please key in Student's Gender (1 is male, 0 is female):");
                    S = new Scanner(System.in);
                    Gender = S.nextInt();
                    System.out.println("Please key in Student's GPA:");
                    S = new Scanner(System.in);
                    GPA = S.nextFloat();

                    CreateStudent(name, RegNum, Gender, GPA);

                    break;
                case 3:
                    int RegNumDelete;

                    // Remove Student
                    System.out.println("Please key in Student's Reg Number (3 Digits):");
                    S = new Scanner(System.in);
                    RegNumDelete = S.nextInt();

                    DeleteStudent(RegNumDelete);

                    break;
                case 4:
                    // Print Aggregated
                    PrintAggregate();

                    break;
                default:

                    break;
            }
        }

        S.close();
    }

    public static void CreateStudent(String name, int RegNum, int Gender, float GPA)
    {
        if (Records.AddStudent(name, RegNum, Gender, GPA))
        {
            System.out.println("Student " + name + " registered successfully");
        }
        else
        {
            System.out.println("Invalid student registration");
        }
    }

    public static void DeleteStudent(int RegNumDelete)
    {
        if (Records.RemoveStudent(RegNumDelete))
        {
            System.out.println("Student with " + RegNumDelete + " deleted successfully");
        }
        else
        {
            System.out.println("Student does no exist");
        }
    }

    public static void PrintAggregate()
    {
        System.out.println("Total Number of students" + Records.GetNumStudents());

        System.out.println("Total Male Students : " + Records.GetMaleStudents() + " Total Female Students : " + Records.GetFemaleStudent());

        System.out.println("Total Average GPA : " + Records.GetAverageGPA());
    }

}
