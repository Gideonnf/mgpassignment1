package e.gideo.javalib;
import java.util.*;

public class RecordManager {
    static List<Student> Records = new LinkedList<>();

    public static boolean AddStudent(String Name, int RegNum, int Gender, float GPA)
    {
        if (Math.floor((Math.log10(RegNum)+1)) > 3)
        {
            return false;
        }
        Student temp = new Student(Name, RegNum, Gender, GPA);
        Records.add(temp);
        return true;
    }

    public static boolean RemoveStudent(int RegNumRemove)
    {
        if (Records.isEmpty())
            return false;

//        for(Iterator<Student> iter = Records.iterator(); iter.hasNext();)                           // Java's way of looking through a link list
//        {
//            Student temp = iter.next();                                                      // Stores it in a temp student
//            if (temp.RegistrationNumber == RegNumRemove)
//            {
//                iter.remove();
//                return true;
//            }
//        }

        for (int i = 0; i < Records.size(); ++i)
        {
            if(Records.get(i).RegistrationNumber == RegNumRemove);
            {
                Records.remove(i);
                return true;
            }
        }
        return false;
    }

    public static String GetStudentList()
    {
        if (Records.isEmpty())
            return "Records is empty";

        String studentList = Records.get(0).name;
        for (int i = 1; i < Records.size(); i++)                                                    // Start at the second element
        {
            studentList += ", " + Records.get(i).name;
        }

        return studentList;
    }

    public static int GetNumStudents()
    {
        if (Records.isEmpty())
            return 0;

        return Records.size();
    }

    public static int GetMaleStudents()
    {
        if (Records.isEmpty())
            return 0;

        int temp = 0;
        for(int i = 0; i < Records.size(); i++)
        {
            if (Records.get(i).Gender == 1)
            {
                temp++;
            }
        }

        return temp;
    }

    public static int GetFemaleStudent()
    {
        if (Records.isEmpty())
            return 0;

        int temp = 0;
        for(int i = 0; i < Records.size(); i++)
        {
            if (Records.get(i).Gender == 0)
            {
                temp++;
            }
        }

        return temp;
    }

    public static float GetAverageGPA()
    {
        if (Records.isEmpty())
            return 0;

        float AverageGPA = 0;
        for(int i = 0; i < Records.size(); i++)
        {
            AverageGPA += Records.get(i).GPA;
        }
        AverageGPA = AverageGPA / Records.size();
        return AverageGPA;
    }


}
